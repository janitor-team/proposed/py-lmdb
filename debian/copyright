Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Contact: Nic Watson <github@nicwatson.org>
Source: https://github.com/jnwatson/py-lmdb

Files: *
Copyright: 2013-2021 The py-lmdb authors
License: OLDAP-2.8

Files: lib/lmdb.h lib/mdb.c
Copyright: 2011-2021 Howard Chu, Symas Corp.
           2009, 2010 Martin Hedenfalk <martin@bzero.se>
License: OLDAP-2.8 and OpenBSD

Files: lib/midl.c
Copyright: 2000-2020 The OpenLDAP Foundation.
 Portions Copyright 2001-2020 Howard Chu, Symas Corp.
 All rights reserved.
License: OLDAP-2.8

Files: lib/win32/inttypes.h lib/win32-stdint/stdint.h
Copyright: 2006-2013 Alexander Chemeris
License: BSD-3-clause~author

License: OLDAP-2.8
 Redistribution and use of this software and associated documentation
 ("Software"), with or without modification, are permitted provided
 that the following conditions are met:
 .
 1. Redistributions in source form must retain copyright statements
    and notices,
 .
 2. Redistributions in binary form must reproduce applicable copyright
    statements and notices, this list of conditions, and the following
    disclaimer in the documentation and/or other materials provided
    with the distribution, and
 .
 3. Redistributions must contain a verbatim copy of this document.
 .
 The OpenLDAP Foundation may revise this license from time to time.
 Each revision is distinguished by a version number.  You may use
 this Software under terms of this license revision or under the
 terms of any subsequent revision of the license.
 .
 THIS SOFTWARE IS PROVIDED BY THE OPENLDAP FOUNDATION AND ITS
 CONTRIBUTORS ``AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT
 SHALL THE OPENLDAP FOUNDATION, ITS CONTRIBUTORS, OR THE AUTHOR(S)
 OR OWNER(S) OF THE SOFTWARE BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 .
 The names of the authors and copyright holders must not be used in
 advertising or otherwise to promote the sale, use or other dealing
 in this Software without specific, written prior permission.  Title
 to copyright in this Software shall at all times remain with copyright
 holders.

License: OpenBSD
 Permission to use, copy, modify, and distribute this software for any
 purpose with or without fee is hereby granted, provided that the above
 copyright notice and this permission notice appear in all copies.
 .
 THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

License: BSD-3-clause~author
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
   1. Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
 .
   2. Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
 .
   3. Neither the name of the product nor the names of its contributors may
      be used to endorse or promote products derived from this software
      without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
